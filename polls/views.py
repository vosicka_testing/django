from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, Http404

# Create your views here.

from .models import Question


def index(request):
    # vezmi prvních 5 nejnovějších
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    # do outputu najoinuj question text z objektu latest_question_list
    # output = ', '.join([q.question_text for q in latest_question_list])
    context = {
        'latest_question_list': latest_question_list,
    }
    return render(request, "polls/index.html", context)


def detail(request, question_id):
    try:
        question = Question.objects.get(pk=question_id)
    except question.DoesNotExist:
        raise Http404("Qiestion does not exists")
    return render(request, "polls/detail.html", {'question': question})


def results(request, question_id):
    return HttpResponse("You are looking at the results of: %s." % question_id)


def vote(request, question_id):
    return HttpResponse("You are voting for question: %s." % question_id)
